SET(SRCROOT ${CMAKE_SOURCE_DIR}/examples/blend)

SET(Blend_Src
    ${SRCROOT}/blend.c
    ${LIBROOT}/test.c
)

ADD_EXECUTABLE(
    Blend
    ${Blend_Src}
)

#TODO: use FindOpenVG
TARGET_LINK_LIBRARIES(
    Blend
    OpenVG
    glut
)


