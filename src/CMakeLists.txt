SET(LIBRARY_OUTPUT_PATH "${CMAKE_BINARY_DIR}/lib")

ADD_DEFINITIONS(
    -DVG_API_EXPORT
    -DHAVE_INTTYPES_H    
)

INCLUDE(FindOpenGL)

FIND_PACKAGE(OpenGL REQUIRED)

INCLUDE_DIRECTORIES(
    ${OPENGL_INCLUDE_DIR}
)

SET(INCROOT ${CMAKE_SOURCE_DIR}/include/VG)
SET(SRCROOT ${CMAKE_SOURCE_DIR}/src)

SET(ShivaVG_Src
    ${SRCROOT}/shArrays.c
    ${SRCROOT}/shArrays.h
    ${SRCROOT}/shContext.c
    ${SRCROOT}/shContext.h
    ${SRCROOT}/shExtensions.c
    ${SRCROOT}/shExtensions.h
    ${SRCROOT}/shGeometry.c
    ${SRCROOT}/shGeometry.h
    ${SRCROOT}/shImage.c
    ${SRCROOT}/shImage.h
    ${SRCROOT}/shPaint.c
    ${SRCROOT}/shPaint.h
    ${SRCROOT}/shParams.c
    ${SRCROOT}/shPath.c
    ${SRCROOT}/shPath.h
    ${SRCROOT}/shPipeline.c
    ${SRCROOT}/shVectors.c
    ${SRCROOT}/shVectors.h
    ${SRCROOT}/shVgu.c
    ${SRCROOT}/GL/glext.h
)

ADD_LIBRARY(
    OpenVG
    ${ShivaVG_Src}
)

TARGET_LINK_LIBRARIES(
    OpenVG
    ${OPENGL_gl_LIBRARY}
    ${OPENGL_glu_LIBRARY}
)

INSTALL(
    TARGETS OpenVG
    RUNTIME DESTINATION bin COMPONENT bin
    LIBRARY DESTINATION lib${LIB_SUFFIX} COMPONENT bin
    ARCHIVE DESTINATION lin${LIB_SUFFIX} COMPONENT devel
)
